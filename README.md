# Installationshinweise
* Erfordert C++17 oder hoeher, SFML 2.5.1, gcc 9.4.0 oder hoeher und make
* Unter Debian/Ubuntu schnelle Installation: `sudo apt install g++ make libsfml-dev`
	* oder einfach das Installationsskript im Repo nutzen
# Kompilieren
* Einfache kompilierung mittels:
```
$> git clone https://gitlab.com/toobi7007/pacsf
$> cd ./pacsf/
$> make
$> ./pacman
```
* Neue kompilierung:
```
$> make clean
$> make 
```
