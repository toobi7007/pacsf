#ifndef _HELP_HPP
#define _HELP_HPP

#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <regex>

#define Expects(x)				\
  if(!(x)) {					\
    std::cerr << __FILE__ << ":"		\
	      << __LINE__ << ": "		\
	      << " Expect failed: "		\
	      << #x << std::endl;		\
    std::exit(1);				\
  }

#define Ensures(x)				\
  if(!(x)) {					\
    std::cerr << __FILE__ << ":"		\
              << __LINE__ << ": "		\
              << " Ensures failed: "		\
              << #x << std::endl;		\
    std::exit(1);				\
  } 

namespace help {
  std::vector<std::string> getInput(std::string);

  std::map<std::string, std::string> parseCSV_Pairs(const std::string);

  std::vector<std::vector<std::string>> parseCSV(const std::string);
}

#endif
