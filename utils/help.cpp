#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <regex>
#include "help.hpp"

namespace help {
  std::vector<std::string> getInput(std::string path) {
    std::vector<std::string> lines;
    std::fstream file(path, std::ios::in);

    std::string zeile;
    Expects(file.is_open());
    while (getline(file, zeile))
      lines.push_back(zeile);

    Ensures(lines.size() > 0);
    return lines;
  }     

  std::map<std::string, std::string> parseCSV_Pairs(const std::string path) {
    std::vector<std::string> lines {getInput(path)};
    Ensures(lines.size() > 0);

    std::map<std::string, std::string> pairs;
    const std::regex wrex(";");
    for(auto &&x : lines) {
      std::sregex_token_iterator itl(std::begin(x), std::end(x), wrex, -1);
      pairs.insert({*itl, *std::next(itl)});
    }
    Ensures(lines.size() == pairs.size());
    return pairs;
  }

  std::vector<std::vector<std::string>> parseCSV(const std::string path) {
    std::vector<std::string> lines {getInput(path)};
    Ensures(lines.size() > 0);
    std::vector<std::vector<std::string>> pairs;
    const std::regex wrex(";");
    for(auto &&x : lines) {
      std::sregex_token_iterator itl(std::begin(x), std::end(x), wrex, -1);
      std::sregex_token_iterator itlend;
      std::vector<std::string> help;
      while(itl != itlend) {
	help.emplace_back(*itl);
	++itl;
      }
      pairs.emplace_back(help);
    }
    Ensures(lines.size() == pairs.size());
    return pairs;
  }
}
