#ifndef PACMAN_H
#include "help.hpp"
#include <SFML/Graphics/RenderWindow.hpp>
#define PACMAN_H

#include <string>
#include <utility>

class pacman {
  private:
  std::pair<int, int> position;
  std::pair<int, int> originalPosition;
  int Direction, Live; // bracuh ich nur für den scheiße PinkenGeist deswegen in dem Kack formate
  bool Pacmangotpowerup;

  public:
  pacman() {
    this->originalPosition = std::pair<int,int>(0, 0);
    this->position = this->originalPosition;
    this->Direction = 1;
    this->Live = 3;
    this->Pacmangotpowerup = false;
  }

  pacman(int posX, int posY) {
    this->originalPosition = std::pair<int, int>(posX, posY);
    this->position = this->originalPosition;
    this->Direction = 1;
    this->Live = 3;
    this->Pacmangotpowerup = false;
  }

  // GETTER 
  int getX() const { return this->position.first; }
  int getY() const { return this->position.second; }
  int getDirection() const { return this->Direction; }
  int getlive() const { return this->Live; }
  bool getPacmanGotPowerUp() const { return this->Pacmangotpowerup; }

  // SETTER
  void setX(int &x) { this->position.first = x; }
  void setY(int &y) { this->position.second = y; }
  void setLive(int &live) { this->Live = live; }
  // Was macht die Funktion?
  void setPacmanGotPowerUp() { Pacmangotpowerup != getPacmanGotPowerUp(); }

  // Aktionen auf Pacman/Spieler
  void reset() { this->position = this->originalPosition;}
  void moveUp() { this->Direction = 3; this->position.second--; }
  void moveDown() { this->Direction= 4; this->position.second++; }
  void moveLeft() { this->Direction= 1; this->position.first--; }
  void moveRight() { this->Direction = 2; this->position.first++; }

  std::string toString() {
    return "Pacman [" + std::to_string(this->position.first)
      + ", " + std::to_string(this->position.second) + "]";
  }
  // Warum keine toString Methode?
  // friend ostream& operator<<(ostream& o, pacman c) {
  //   o << "Pacman [" << c.x << "," << c.y << "]";
  //   return o;
  // }
};

#endif
