#include <iostream>
#include "Ghost.hpp"
#include "help.hpp"

ghost::ghost(const int &posX, const int &posY, const std::string &name) {
  this->originalPosition = std::pair<int, int>(posX, posY);
  this->position = this->originalPosition;
  this->target = std::pair<int, int>(0, 0);

  this->direction = STOP;
  this->state = "Chase";
  this->name = name;
}

void ghost::berechnungTarget(int pacmanX, int pacmanY, std::string name,
                             int pacmanDirection) {
  if (state == "Eaten") {
    this->target = std::pair<int, int>(33, 10);
    direction = eDir(1);
    return;
  }
  if (state == "FRIGHTENED") {
    //(rand() % 21) + 1);
    this->target = std::pair<int, int>(((std::rand() % 21) + 1),
				       ((std::rand() % 31) + 17));
  }
  if (state == "Scatter") {
    if (this->getName() == "BLINKY") {
      this->target=std::pair<int, int>(17, 3);
             
    }
    if (this->getName() == "PINKY") {
      this->target = std::pair<int, int>(47, 3);
    }
    if (this->getName() == "CLYDE") {
      this->target = std::pair<int, int>(17, 24);
    }
    if (this->getName() == "INKY") {
      this->target = std::pair<int, int>(47, 24);
    }
  }
  if (state == "Chase") {
    if (name == "BLINKY") {
      this->target = std::pair<int, int>(pacmanX, pacmanY);
    }
    if (name == "PINKY") {
      if (pacmanDirection == 4)
	this->target = std::pair<int, int>(pacmanX, pacmanY+4);
      if (pacmanDirection == 3)
	this->target = std::pair<int, int>(pacmanX -4, pacmanY -4);// so ist original getreu
      if (pacmanDirection == 2)
	this->target = std::pair<int, int>(pacmanX + 4, pacmanY);
      if (pacmanDirection == 1)
	this->target = std::pair<int, int>(pacmanX -4, pacmanY);
      //{ STOP = 0, LEFT = 1, RIGHT = 2, UP = 3, DOWN = 4 }; wieder zufaul und merken kann ich mir auch nichts
    }
    if (name == "CLYDE") {
      if (8 >= abs(pacmanX - (this->getX())) && 8 >= abs(pacmanY - (this->getY())))
	this->target = std::pair<int, int>(pacmanX, pacmanY);
      else
	this->target = std::pair<int, int>(17, 24);
    }
    if (name == "INKY") {// ich dachte Pinky war ne Bastared aber INKY ist noch schlimmer 
      this->berechnungTarget(pacmanX, pacmanY, "BLINKY", pacmanDirection);
      if (pacmanDirection == 4)
	this->target = std::pair<int, int>(abs((pacmanX)-getTargetX()),(abs((pacmanY + 2) - getTargetY())));
      if (pacmanDirection == 3)
	this->target = std::pair<int, int>(abs((pacmanX - 2)-getTargetX()), (abs((pacmanY - 2) - getTargetY())));
      if (pacmanDirection == 2)
	this->target = std::pair<int, int>(abs((pacmanX + 2)-getTargetX()), (abs((pacmanY) - getTargetY())));
      if (pacmanDirection == 1)
	this->target = std::pair<int, int>(abs((pacmanX -2)-getTargetX()), (abs((pacmanY) - getTargetY())));
    }
  }
}

void blinky::drawMe(std::vector<std::vector<std::string>> &map){
  std::pair<int, int> position(this->getPosition());
  Expects(map.size() > position.second);
  Expects(map.at(position.second).size() > position.first);

  map.at(position.second).at(position.first) = "blinky";
}

void pinky::drawMe(std::vector<std::vector<std::string>> &map){
  std::pair<int, int> position(this->getPosition());
  Expects(map.size() > position.second);
  Expects(map.at(position.second).size() > position.first);

  map.at(position.second).at(position.first) = "pinky";
}

void clyde::drawMe(std::vector<std::vector<std::string>> &map){
  std::pair<int, int> position(this->getPosition());
  Expects(map.size() > position.second);
  Expects(map.at(position.second).size() > position.first);

  map.at(position.second).at(position.first) = "clyde";
}

void inky::drawMe(std::vector<std::vector<std::string>> &map){
  std::pair<int, int> position(this->getPosition());
  Expects(map.size() > position.second);
  Expects(map.at(position.second).size() > position.first);

  map.at(position.second).at(position.first) = "inky";
}
