#ifndef GHOST_H
#define GHOST_H

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <SFML/Graphics.hpp>
enum eDir { STOP = 0, LEFT = 1, RIGHT = 2, UP = 3, DOWN = 4 };

class ghost {
private:
  std::pair<int, int> position;
  std::pair<int, int> originalPosition;
  std::pair<int, int> target;
  std::string state;
  std::string name;
  eDir direction;

public:
  ghost(const int& posX, const int& posY, const std::string& name);

  // GETTER
  std::string getState() const { return this->state; }
  std::string getName() const{ return this->name; }
  std::pair<int, int> getPosition() const { return this->position; }
  int getX() const { return this->position.first; }
  int getY() const { return this->position.second; }
  int getTargetX() const { return this->target.first; }
  int getTargetY() const { return this->target.second; }
  eDir getDirection() const { return this->direction; }

  // SETTER
  void setState(std::string state) { this->state = state; }
  void setName(std::string name) { this->name = name; }
  void setTargetX(int target)  { this->target.first = target; }
  void setTargetY(int target)  { this->target.second = target; }

  void Reset() {
    this->position = this->originalPosition;
    this->direction = STOP;
  }

  void changeDirection(const eDir& d) {
    this->direction = d;
  }
  void randomDirection() {
    this->direction = static_cast<eDir>(((rand() % 4) + 1));
  }

  void berechnungTarget(int pacmanX, int pacmanY,
  			std::string name, int pacmanDirection);
};

class blinky : public ghost {
public:
  blinky() : blinky(1, 1, "BLINKY") { }

  blinky(const int& posX, const int& posY, const std::string& name) :
    ghost(posX, posY, "BLINKY") { }

  void drawMe(std::vector<std::vector<std::string>> &map);
};

class pinky : public ghost {
public:
  pinky() : pinky(1, 1, "PINKY") { }

  pinky(const int& posX, const int& posY, const std::string& name) :
    ghost(posX, posY, "PINKY") { }
  
  void drawMe(std::vector<std::vector<std::string>> &map);
};

class clyde : public ghost {
public:
  clyde() : clyde(1, 1, "CLYDE") { }

  clyde(const int& posX, const int& posY, const std::string& name) :
    ghost(posX, posY, "CLYDE") { }
  
  void drawMe(std::vector<std::vector<std::string>> &map);
};

class inky : public ghost {
public:
  inky() : inky(1, 1, "INKY") { }

  inky(const int& posX, const int& posY, const std::string& name) :
    ghost(posX, posY, "INKY") { }
  
  void drawMe(std::vector<std::vector<std::string>> &map);
};

#endif
