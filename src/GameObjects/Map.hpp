#ifndef MAP_H
#define MAP_H

#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

namespace gm {
/**
   Repraesentiert die Map virtuell.
   Liest eine Map in Form einer .txt Datei ein.
 */
  class map {
  private:
    std::vector<std::string> values;

  public:
    map(const std::string &path) {
      values = help::getInput(path);
    }

    size_t getSizeX() const {
      return this->values.at(0).size();
    }

    size_t getSizeY() const {
      return this->values.size();
    }

    /*
      Gibt die Anzahl der DOTS im aktuellen Spielfeld zurueck.
     */
    size_t getDOTS() const {
      size_t help {0};
      for(const auto& x : this->values) {
	help += std::count_if(x.begin(),
			      x.end(),
			      [](const char &y){
				return y == '1' ? true : false;
			      });
      }
      return help;
    }

    /*
      Ueberprueft mit ob es im Feld ist.
      @param ort Die zu suchende Position pair<x, y>
      @return Wert im Feld
     */
    char getMapChar(std::pair<int, int> ort) const {
      Expects(ort.first > 0 && ort.second > 0);
      Expects(ort.second < this->values.size());
      Expects(this->values.at(ort.second).size() > ort.first);

      return this->values.at(ort.second).at(ort.first);
    }

    /*
      Setzt den Wert im virtuellen Spielfeld.
      @param x Entspricht dem x Wert
      @param y Entspricht dem y Wert
      @param value Entspricht dem Wert welcher angenommen werden soll
     */
    void setMapChar(const size_t &x, const size_t &y, const char &value) {
      Expects(x > 0 && y > 0);
      Expects(y < this->values.size());
      Expects(x < this->values.at(y).size());
      this->values.at(y).at(x) = value;
    }

    /*
      Gibt die komplette Map als Array zurueck.
      @return Das komplette Spielfeld
     */
    std::vector<std::string> getMap() const { return this->values; }

    std::string toString() const {
      return "Es funktioniert";
    }
  };
} 

#endif
