#include "GameManager.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Color.hpp>
#include <typeinfo>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <regex>
#include <map>
#include <utility>
#include <variant>
#include "help.hpp"


GameManager::GameManager() {
  // Deklaration der Spielfeldgroesse
  this->playgroundSize = std::pair<int, int>(31, 22);

  // Deklaration der Geister
  this->geister.emplace_back(blinky(15, 7, "BLINKY"));
  this->geister.emplace_back(pinky(14, 9, "PINKY"));
  this->geister.emplace_back(inky(15, 9, "INKY"));
  this->geister.emplace_back(clyde(16, 9, "CLYDE"));
}

std::map<std::string, sf::Texture> GameManager::generateAssets() {
  std::map<std::string, sf::Texture> texturen;
  sf::Texture texture;
  auto csv {help::parseCSV_Pairs("assets/namen.csv")};
  for(auto x {std::begin(csv)}; x != std::end(csv); ++x) {
    texture.loadFromFile("assets/" + x->second);
    texturen.insert({x->first, texture});
  }
  return texturen;
}

std::vector<std::vector<std::string>> GameManager::parseMap(const std::string path) {
  auto map {help::parseCSV(path)};
  Expects(map.size() == this->playgroundSize.second);
  return map;
}

void GameManager::drawMap(sf::RenderWindow& window) {
  const sf::Vector2f tilesize(31.f, 31.f);
  std::vector<std::vector<sf::RectangleShape>> shape(this->playgroundSize.second);
  for(auto&& x : shape) x.resize(this->playgroundSize.first, sf::RectangleShape());

#ifdef __linux__
  auto karte {this->parseMap("src/feld.txt")};
#elif _WIN32
  auto karte {this->parseMap("C:\\Users\\hende\\Desktop\\C++\\pacmansfml\\pacmansfml\\feld.txt")};
#endif

  // Laden der Texturen aus der "name.csv"
  const auto texturen {this->generateAssets()};

  // Setze die aktuellen Positionen der Geister
  for(auto& x : this->geister) {
    std::visit([&karte] (auto&& geist) {
		 geist.drawMe(karte);
	       }, x);
  }

  for(auto y {std::begin(shape)}; y != std::end(shape); ++y) {
    auto karteY {std::next(std::begin(karte), std::distance(std::begin(shape), y))};

    for(auto x {std::begin(*y)}; x != std::end(*y); ++x) {
      auto karteX {std::next(std::begin(*karteY), std::distance(std::begin(*y), x))};

      x->setSize(tilesize);
      // Expects(texturen.find(*karteX) != std::end(texturen) or *karteX == "#");
      if(texturen.find(*karteX) != std::end(texturen))
	x->setTexture(&texturen.at(*karteX));
      else {
	if(*karteX == "#")
	  x->setFillColor(sf::Color::White);
	else if(*karteX == "blinky")
	  x->setFillColor(sf::Color::Red);
	else if(*karteX == "inky")
	  x->setFillColor(sf::Color::Cyan);
	else if(*karteX == "pinky")
	  x->setFillColor(sf::Color::Magenta);
	else if(*karteX == "clyde")
	  x->setFillColor(sf::Color::Yellow);
	else
	  x->setFillColor(sf::Color::Black);

      }
      x->setPosition(tilesize.x * std::distance(std::begin(*y), x),
		     tilesize.y * std::distance(std::begin(shape), y));
      window.draw(*x);
    }
  }
  window.getView();
}

void GameManager::Run() {
  sf::RenderWindow window(sf::VideoMode(31.0 * this->playgroundSize.first,
					31.0 * this->playgroundSize.second),
			  "Pacman");
       
  while (window.isOpen()) {
    sf::Event event;

    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed)
	window.close();
    }

    window.clear();
    this->drawMap(window);
    window.display();
  }
}
