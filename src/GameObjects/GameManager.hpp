#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H
#include <string>
#include <SFML/Graphics.hpp>
#include <variant>
#include "PlayerObjects/Ghost.hpp"

class GameManager {
private:
  std::pair<int, int> playgroundSize;
  std::vector<std::vector<std::string>> defaultMap;
  std::vector<std::variant<blinky, pinky, inky, clyde>> geister;

public:
  GameManager();

private:
  std::map<std::string, sf::Texture> generateAssets();
  std::vector<std::vector<std::string>> parseMap(const std::string);

public:
  void drawMap(sf::RenderWindow&);
  void Run();
};

#endif
