NAME = pacman
VPATH = src/
LIBS = -lstdc++ -lsfml-graphics -lsfml-window -lsfml-system
STANDARD = -std=c++17 
OBJDIR = build
INC = -Iutils -Isrc

$(NAME): main.o GameManager.o ghost.o help.o
	g++ $(STANDARD) -g -o $(NAME) main.o Ghost.o GameManager.o help.o $(LIBS)

GameManager.o : GameObjects/GameManager.cpp 
	g++ $(STANDARD) -g $(INC) -c src/GameObjects/GameManager.cpp

main.o: main.cxx GameObjects/GameManager.hpp
	g++ $(STANDARD) -g $(INC) -c src/main.cxx

ghost.o: PlayerObjects/Ghost.cxx
	g++ $(STANDARD) -g $(INC) -c src/PlayerObjects/Ghost.cxx

help.o: ../utils/help.cpp
	g++ $(STANDARD) -g $(INC) -c utils/help.cpp

clean:
	rm -rf *.o $(NAME)
